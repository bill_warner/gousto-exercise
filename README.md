# Gousto Exercise
## Scope
Design a model to calculate menu conversion rates at Gousto.

## Model DB Diagram
![DB Diagram](https://i.ibb.co/RNM0GXc/Gousto-Flow-Diagram-1.png)  
[Link](https://app.creately.com/diagram/3AkZKdhTvg4/view)

## Scripts
- sessions.sql
- user_mapping.sql
- menu_conversion.sql