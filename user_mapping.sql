
WITH staging_1 AS (
SELECT
	DISTINCT
	events.cookie_id
	, events.user_id

FROM {{ref('events_data')}} AS events
WHERE events.user_id IS NOT NULL
)

, staging_2 AS (
SELECT
	stage_1.cookie_id
	, stage_1.user_id
	, COUNT(stage_1.user_id) OVER(PARTITION BY stage_1.cookie_id) user_count

FROM staging_1 AS stage_1
)

SELECT *
	, md5(ROW(stage_2.cookie_id, stage_2.user_id)::TEXT) _pk

FROM staging_2 AS stage_2

