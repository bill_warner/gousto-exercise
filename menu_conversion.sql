
-- Take first timestamp per global session as session start. 
-- Collapse down by removing event_name
WITH staging_1 AS (
SELECT
	DISTINCT 
	sessions.cookie_id
	, sessions.user_id
	, MIN(sessions.timestamp) OVER(PARTITION BY sessions.global_session_id) AS session_start_timestamp
	, sessions.global_session_id
	, sessions.cookie_session_id
	, sessions.page

FROM {{ref('sessions')}} sessions
WHERE sessions.session_ended
)

-- Join in device and user details.
-- Boolean flags for menu visiting & order confirmation
SELECT
	stage_1.cookie_id
	, stage_1.user_id
	, stage_1.session_start_timestamp
	, stage_1.global_session_id
	, stage_1.cookie_session_id
	, users.county AS user_county
	, users.country AS user_country
	, users.marketing_channel AS user_marketing_channel
	, users.affiliate AS user_affiliate
	, device.device_type
	, device.browser_type
	, SUM(CASE WHEN split_part(stage_1.page,'/',2) = 'menu' THEN 1 ELSE 0 END)::int::boolean visited_menu
	, SUM(CASE WHEN split_part(stage_1.page,'/',2) = 'order-confirmation' THEN 1 ELSE 0 END)::int::boolean converted

FROM staging_1 AS stage_1
LEFT JOIN {{ref('user_accounts')}} AS users ON
stage_1.user_id = users.user_id
LEFT JOIN {{ref('device_details')}} AS device ON
stage_1.cookie_id = device.cookie_id

GROUP BY 1,2,3,4,5,6,7,8,9,10,11
