
-- Look back for last event timestamp per cookie
WITH staging_1 AS (
SELECT
	events.cookie_id
	, events.timestamp
	, events.event_name
	, events.page
	, events.user_id
	, LAG(events.timestamp) OVER (PARTITION BY events.cookie_id ORDER BY events.timestamp) AS last_timestamp

FROM {{ref('raw_events')}} AS events
)

-- Declare start of new session if no previous event 
-- or time between events >=30 mins
, staging_2 AS (
SELECT
	stage_1.cookie_id
	, stage_1.timestamp
	, stage_1.last_timestamp
	, CASE WHEN 
		EXTRACT('EPOCH' FROM stage_1.timestamp) - EXTRACT('EPOCH' FROM stage_1.last_timestamp) >= 1800 --30mins
		OR stage_1.last_timestamp IS NULL
		THEN 1 ELSE 0 END new_session
	, stage_1.event_name
	, stage_1.page
	, stage_1.user_id

FROM staging_1 AS stage_1
)

-- Declare global and cookie level session ids
, staging_3 AS (
SELECT
	stage_2.cookie_id
	, stage_2.timestamp
	, stage_2.last_timestamp
	, SUM(stage_2.new_session) OVER (ORDER BY stage_2.cookie_id, stage_2.timestamp) AS global_session_id
    , SUM(stage_2.new_session) OVER (PARTITION BY stage_2.cookie_id ORDER BY stage_2.timestamp) AS cookie_session_id
    , stage_2.event_name
	, stage_2.page
	, stage_2.user_id

FROM staging_2 AS stage_2
)

-- Lookup user id from any events within session. Assumes 1 user per session.
, staging_4 AS (
SELECT
	stage_3.cookie_id
	, stage_3.timestamp
	, MAX(stage_3.timestamp) OVER(PARTITION BY stage_3.global_session_id) max_timestamp
	, stage_3.global_session_id
	, stage_3.cookie_session_id
	, stage_3.event_name
	, stage_3.page
	, CASE WHEN stage_3.user_id IS NULL THEN FALSE ELSE TRUE END logged_in
 	, FIRST_VALUE(stage_3.user_id) OVER (
 		PARTITION BY stage_3.cookie_id
 					, stage_3.cookie_session_id 
 		ORDER BY CASE WHEN stage_3.user_id IS NOT NULL THEN stage_3.user_id END DESC NULLS LAST) user_id --no 'ignore nulls' in postgres 

FROM staging_3 AS stage_3
ORDER BY stage_3.cookie_id, stage_3.timestamp
)

-- Join in user mappings to get assumed user_id for non-logged in sessions
-- Create PK for testing uniqueness + non null contraints.
SELECT
	stage_4.cookie_id
	, stage_4.timestamp
	, CASE WHEN 
		EXTRACT('EPOCH' FROM NOW()) - EXTRACT('EPOCH' FROM stage_4.max_timestamp) >= 1800
		THEN TRUE ELSE FALSE 
	  END session_ended
	, stage_4.global_session_id
	, stage_4.cookie_session_id
	, stage_4.event_name
	, stage_4.page
	, stage_4.logged_in
	, CASE WHEN user_mapping.user_id IS NOT NULL THEN TRUE ELSE FALSE END user_mapping
	, COALESCE(stage_4.user_id, user_mapping.user_id) AS user_id
	, md5(ROW(stage_4.cookie_id, stage_4.timestamp)::TEXT) _pk

FROM staging_4 AS stage_4
LEFT JOIN {{ref('user_mapping')}} AS user_mapping ON
stage_4.cookie_id = user_mapping.cookie_id
AND user_mapping.user_count = 1 --Accept user mapping where only one known user per device. If multiple users, ignore.
AND stage_4.user_id IS NULL

